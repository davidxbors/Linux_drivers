#include <linux/init.h>
#include <linux/module.h>

int module04_init(void){
  printk(KERN_ALERT "init\n!");
  return 0;
}

void module04_exit(void){
  printk(KERN_ALERT "exit\n!");
}

module_init(module04_init);
module_exit(module04_exit);
