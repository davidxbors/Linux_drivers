#include <linux/init.h>
#include <linux/module.h>

//MODULE_LICENSE(str) -> tells the kernel what license the module has
//not using it taints the kernel
//don't have acces to important functionalities
//like working with usb
MODULE_LICENSE("GPL2");

// putting __init before functions makes them to be del from
// kernel ram after module_init excuted
// !put __init / __initdata just before variables/functions used in module_init
// the same goes for __exit

__initdata int count = 5;
__exitdata int counter = 4;

__init static void hello_f(void){
  printk(KERN_ALERT "Hello!\n");
}

__exit static void bye_f(void){
  printk(KERN_ALERT "Bye!\n");
}

__init static int module05_init(void){
  int index;
  printk(KERN_ALERT "Module 06 says: Tom e gras!\n");
  for(index = 0; index < count; index++)
    hello_f();
  return 0;
}

__exit static void module05_exit(void){
  int index;
  printk(KERN_ALERT "Module 06 says: Tom e urat!\n");
  for(index = 0; index < counter; index++)
    bye_f();
}

module_init(module05_init);
module_exit(module05_exit);
