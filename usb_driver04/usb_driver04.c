/*
NOT WORKING
*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>

/* probe function */
static int davids_probe(struct usb_interface *intf, const struct usb_device_id *id){
	//setup the endpoint information
	//use only first bulk-in and bulk-out endpoints
	int i;
	auto iface_desc = intf->cur_altsetting;
	for(i = 0; i < iface_desc->desc.bNumEndpoints; ++i){
		auto endpoint = &iface_desc->endpoint[i].desc;

		if(!dev->bulk_in_endpointAddr && (endpoint->bEndpointAddress & USB_DIR_IN) && ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)) {
			//we found a bulk in endpoint
			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_in_size = buffer_size;
			dev->bulk_in_endpointAddr = endpoint->bEndpointAddress;
			dev->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
			if(!dev->bulk_in_buffer){
				printk(KERN_ALERT "Error could not allocate bulk_in_buffer\n");
			}
		}
		if(!dev->bulk_out_endpointAddr && !(endpoint->bEndpointAddress & USB_DIR_IN) && ((endpoint->bmAttributes & USB_ENPOINT_XFERTYPE_MASK) == USB_ENPOINT_XFER_BULK)){
			//we found a bulk out endpoint
			dev->bulk_out_endpointAddr = endpoint->bEndpointAddress;
		}	
	}
	if(!(dev->bulk_in_endpointAddr && dev->bulk_out_endpointAddr)){
		printk(KERN_ALERT "could not find both bulk-in and bulk-out endpoints\n");
	} else {
		printk(KERN_INFO "BULK IN: %04x, BULK OUT: %04x", dev->bulk_in_endpointAddr, dev->bulk_out_endpointAddr);
	}
	printk(KERN_INFO "Usb plugged in... David's driver is taking care of this\n");
	return 0;
}

/* disc function */
static void davids_disconnect(struct usb_interface *intf){
	printk(KERN_INFO "Usb plugged out... Goodbye!\n");
}

/* creating the device id table */
/* vendorId, productId */
static struct usb_device_id davids_table [] = {
	{ USB_DEVICE(0x058f, 0x6387) },
	/* if you want your driver to be called for any usb dev */
	/* { .driver_info = 42 }, */
	{ } /*TERMINATING THE ENTRY*/
};
MODULE_DEVICE_TABLE(usb, davids_table);

/*create the usb driver struct*/
static struct usb_driver davids_driver = {
	//.owner = THIS_MODULE,
	.name = "davids usb driver",
	.id_table = davids_table,
	.probe = davids_probe,
	.disconnect = davids_disconnect,
};

static int __init usb_driver04_init(void){

	//register the driver with the usb subsystem
	int result;
	result = usb_register(&davids_driver);
	if(result)
		printk(KERN_ALERT "usb register failed, error %d", result);

	return result;
}

static void __exit usb_driver04_exit(void){
	//deregister the driver from the usb subsystem
	usb_deregister(&davids_driver);
}

module_init(usb_driver04_init);
module_exit(usb_driver04_exit);
MODULE_LICENSE("GPL");
