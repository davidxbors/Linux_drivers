#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("GPL2");

//VERY IMPORTANT first insmod the symbol_exporter
void exported_function(void);

__init static int module07_init(void){
  printk(KERN_ALERT "Module 07 says: Tom e gras!\n");
  exported_function();
  return 0;
}

__exit static void module07_exit(void){
  printk(KERN_ALERT "Module 07 says: Tom e urat!\n");
}

module_init(module07_init);
module_exit(module07_exit);
