#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("GPL2");

//normal function
static void exported_function(void){
  printk(KERN_ALERT "Inside %s\n", __FUNCTION__);
}
//making it exportable by setting it so with the macro
EXPORT_SYMBOL(exported_function);

__init static int module05_init(void){
  printk(KERN_ALERT "Symbol exporter says: Ready to export!\n");
  return 0;
}

__exit static void module05_exit(void){
  printk(KERN_ALERT "Symbol exporter says: Bye!\n");
}

module_init(module05_init);
module_exit(module05_exit);
