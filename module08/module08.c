#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL2");

__initdata int count = 1;

// module_param(var name to be a param, type of var, default value)
//!!! if value already defined and user doesnt give any param
// value remains as declared (changes from kernel version to kernel version)
module_param(count, int, 0);

__init static int module08_init(void){
  int index;
  printk(KERN_ALERT "Module08 entry...\n");
  for(index = 0; index < count; index++)
    printk(KERN_ALERT "Index: %d\n", index);
  return 0;
}

__exit static void module08_exit(void){
  printk(KERN_ALERT "Module08 exit...\n");
}

module_init(module08_init);
module_exit(module08_exit);
