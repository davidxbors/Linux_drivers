#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/fs.h>
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/seq_file.h>
#include <linux/cdev.h>
//#include <asm/system.h>		/* cli(), *_flags */
//#include <asm/uaccess.h>	


/*open function*/
int fopen(struct inode *inode, struct file *filp){
	struct scull_dev *dev; //device info
	
	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	filp->private_data = dev;
	
	//trim to 0 the length of the device if open was write-only
	if( (filp->flags & O_ACCMODE) == O_WRONLY ){
		scull_trim(dev);
	}
	return 0; //succes
}

/*close function*/
int fclose(struct inode *inode, struct file *filp){
	printk(KERN_INFO "David's -> File closed...");
	return 0;
}

/*read function*/
ssize_t fread(struct file *flip, char __user *buf, size_t count, loff_t *f_pos){
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr; //the first listitem
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum *qset; //how many bytes in the listitem
	int item, s_pos, q_pos, rest;
	ssize_t retval = 0;

	if(down_interruptible(&dev->sem))
		return -ERESTARTSYS;
	if(*f_pos >= dev->size)
		count = dev->size - *f_pos;
	
	/*find listitem, qset index, and offset in the quantum*/
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;
	
	/*follow the list up to the right position*/
	dptr = scull_follow(dev, item);

	if(dptr == NULL || !dptr->data || !dptr->data[s_pos])
		goto out;
	
	//read-only up to the end of this quantum
	if(count > quantum - q_pos)
		count = quantum - q_pos;

	if(copy_to_user(buf, dptr->data[s_pos] + q_pos, count)){
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;

	out:
	  up(&dev->sem);
	  return retval;
}

/*write function*/
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos){
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr; //the first listitem
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum *qset; //how many bytes in the listitem
	int item, s_pos, q_pos, rest;
	ssize_t retval = -ENOMEM;

	if(down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	/*find listitem, qset index, and offset in the quantum*/
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;

	/*follow the list up to the right position*/
	dptr = scull_follow(dev, item);
	if(dptr == NULL)
		goto out;
	if(!dptr->data){
		dptr->data = kmalloc(qset *sizeof(char *), GFP_KERNEL);
		if(!dptr->data)
			goto out;
		memset(dptr->data, 0, qset * sizeof(char *));
	}
	if(!dptr->data[s_pos]){
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if(!dptr->data[s_pos])
			goto out;
	}
	if(count > quantum - q_pos)
		count = quantum - q_pos;

	if(copy_from_user(dptr->data[s_pos]+q_pos, buf, count)){
		retval = -EFAULT;
		goto out;
	}

	*f_pos += count;
	retval = count;

	//update size
	if(dev->size < *f_pos)
		dev->size = *f_pos;

	out:
	  up(&dev->sem);
	  return retval;
}

/*file operations driver*/
struct file_operations dvd_fops = {
	.owner = THIS_MODULE,
	.read = fread,
	.write = fwrite,
	.open = fopen,
	.release = frelease,
};

/* probe function */
static int davids_probe(struct usb_interface *intf, const struct usb_device_id *id){
	printk(KERN_INFO "Usb plugged in... David's driver is taking care of this\n");
	return 0;
}


/* disc function */
static void davids_disconnect(struct usb_interface *intf){
	printk(KERN_INFO "Usb plugged out... Goodbye!\n");
}


/* creating the device id table */
/* vendorId, productId */
static struct usb_device_id davids_table [] = {
	{ USB_DEVICE(0x058f, 0x6387) },
	/* if you want your driver to be called for any usb dev */
	/* { .driver_info = 42 }, */
	{ } /*TERMINATING THE ENTRY*/
};
MODULE_DEVICE_TABLE(usb, davids_table);


/*create the usb driver struct*/
static struct usb_driver davids_driver = {
	//.owner = THIS_MODULE,
	.name = "davids usb driver",
	.id_table = davids_table,
	.probe = davids_probe,
	.disconnect = davids_disconnect,
	.minor = USB_SKEL_MINOR,
	.fops = &dvd_fops,
};


static int __init usb_driver05_init(void){

	//register the driver with the usb subsystem
	int result;
	result = usb_register(&davids_driver);
	if(result)
		printk(KERN_ALERT "usb register failed, error %d", result);

	return result;
}


static void __exit usb_driver05_exit(void){
	//deregister the driver from the usb subsystem
	usb_deregister(&davids_driver);
}

module_init(usb_driver05_init);
module_exit(usb_driver05_exit);
MODULE_LICENSE("GPL");
