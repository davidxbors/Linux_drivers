#include <linux/init.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/fs.h>

ssize_t x_read (struct file *pfile, char __user *buffer, size_t length, loff_t *offset){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}
ssize_t x_write (struct file *pfile, const char __user *buffer, size_t length, loff_t *offset){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return length;
}
int x_open (struct inode *pinode, struct file *pfile){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}
int x_close (struct inode *pinode, struct file *pfile){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}

struct file_operations pen_fops = {
  .owner   = THIS_MODULE,
  .open    = x_open,
  .read    = x_read,
  .write   = x_write,
  .release = x_close,
};

//usb device id
//vendor:product (get with lsusb)
static struct usb_device_id pen_table[] = {
	//058f:6387
	{ USB_DEVICE(0x058f, 0x6387) },
	{} /*terminating entry*/
};

MODULE_DEVICE_TABLE(usb, pen_table);

//probe fction
//called on dev if no other driver called it already
//sudo rmmod uas usb_storage !!!
static int pen_probe(struct usb_interface *interface, const struct usb_device_id *id){
	printk(KERN_INFO "David's driver is taking care of this... %04X:%04X plugged in\n", id->idVendor, id->idProduct);
	return 0; //tell linux we will handle the usb
}

//disconnect fction
static void pen_disconnect(struct usb_interface *interface){
	printk(KERN_INFO "Pen drive removed...\n");
}

static struct usb_driver pen_driver = {
	.name = "udd",
	.id_table = pen_table,
	.probe = pen_probe,
	.disconnect = pen_disconnect,
	.fops = &pen_fops,
};

static int udd_entry(void){
  usb_register(&pen_driver);
  printk(KERN_ALERT "Inside the %s function!\n", __FUNCTION__);
  return 0;
}

static void udd_exit(void){
  usb_deregister(&pen_driver);
  printk(KERN_ALERT "Inside the %s function!\n", __FUNCTION__);
}

module_init(udd_entry);
module_exit(udd_exit);
MODULE_LICENSE("GPL");
