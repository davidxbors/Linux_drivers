#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/usb.h>
#include <linux/init.h>

#define BULK_EP_OUT 0x01
#define BULK_EP_IN 0x82
#define MAX_PKT_SIZE 512
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

MODULE_LICENSE("GPL");

static int pen_open(struct inode *i, struct file *f){
	return 0;
}

static int pen_close(struct inode *i, struct file *f){
	return 0;
}

static ssize_t pen_read(struct file *f, char __user *buf, size_t cnt, loff_t *off){
	int retval, read_cnt;
	//read data from the bulk endpoint
	retval = usb_bulk_msg(device, usb_rcvbulkpipe(device, BULK_EP_IN), bulk_buf, MAX_PKT_SIZE, &read_cnt, 5000);
	if(retval){
		printk(KERN_ERR "Bulk msg returned %d\n", retval);
		return retval;
	}
	if(copy_to_user(buf, bulk_buf, MIN(cnt, read_cnt))){
		return -EFAULT;
	}
	return MIN(cnt, read_cnt);
}

static ssize_t pen_write(struct file *f, const char __user *buf, size_t cnt, loff_t *off){
	int retval, wrote_cnt = MIN(cnt, MAX_PKT_SIZE);

	if(copy_from_user(bulk_buf, buf, MIN(cnt, MAX_PKT_SIZE))){
		return -EFAULT;
	}

	//write the data into the bulk endpoint
	retval = usb_bulk_msg(device, usb_sndbulkpipe(device, BULK_EP_OUT), bulk_buf, MIN(cnt, MAX_PKT_SIZE), &wrote_cnt, 5000);
	if(retval){
		printk(KERN_ERR "Bulk msg returned %d\n", retval);
		return retval;
	}
	return wrote_cnt;
}

//defining fops for the usb dev
static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = pen_open,
	.release = pen_close,
	.read = pen_read,
	.write = pen_write
};

//usb device id
//vendor:product (get with lsusb)
static struct usb_device_id pen_table[] = {
	//058f:6387
	{ USB_DEVICE(0x058f, 0x6387) },
	{} /*terminating entry*/
};

MODULE_DEVICE_TABLE(usb, pen_table);

//probe fction
//called on dev if no other driver called it already
//sudo rmmod uas usb_storage !!!
static int pen_probe(struct usb_interface *interface, const struct usb_device_id *id){
	printk(KERN_INFO "David's driver is taking care of this... %04X:%04X plugged in\n", id->idVendor, id->idProduct);
	int retval;

	device = interface_to_usbdev(interface);
	
	class.name = "usb/pen%d";
	class.fops = &fops;
	if((retval = usb_register(interface, &class)) < 0){
		/*smth prevented us from registering the dev*/
		printk(KERN_ERR "Not able to get a minor for this dev...");
	} else {
		printk(KERN_INFO "Minor obtained: %d\n", interface->minor);
	}
	return retval; //tell linux if we will handle the usb (0 means YES)
}

//disconnect fction
static void pen_disconnect(struct usb_interface *interface){
	printk(KERN_INFO "Pen drive removed...\n");
}

//usb driver intialization
/*{
.name = name of the driver
.id_table = a table with the vendor && product id
.probe = fnction to be called when usb inserted into computer
.disconnect = fction to be called when usb out of the computer
}
*/
static struct usb_driver pen_driver = {
	.name = "David's usb driver",
	.id_table = pen_table,
	.probe = pen_probe,
	.disconnect = pen_disconnect
};

//initialize module
//register as a driver
static int __init pen_driver_init(void){
	printk("David's usb driver initialized...");
	printk("\tStarting register process...");
	usb_register(&pen_driver);
	printk("\tRegistration is complete...\n");
	return 0;
}

//close module
//deregister as a driver
static void __exit pen_driver_exit(void){
	printk("David's usb driver closing...");
	printk("\tStarting unregister process...");
	usb_deregister(&pen_driver);
	printk("\tUnregistration is complete...\n");
}

module_init(pen_driver_init);
module_exit(pen_driver_exit);
