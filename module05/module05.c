#include <linux/init.h>
#include <linux/module.h>

static int module05_init(void){
  printk(KERN_ALERT "Module 05 says: Tom e gras!\n");
  return 0;
}

static void module05_exit(void){
  printk(KERN_ALERT "Module 05 says: Tom e urat!\n");
}

module_init(module05_init);
module_exit(module05_exit);
