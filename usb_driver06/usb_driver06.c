/*
it gets all the data about an usb pen drive that u need
*/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>

static struct usb_device *device;

static int pen_probe(struct usb_interface *interface,
				 const struct usb_device_id *id){
	struct usb_host_interface *iface_desc;
	struct usb_endpoint_descriptor *endpoint;
	int i;

	iface_desc = interface->cur_altsetting;
	printk(KERN_ALERT "Pen i/f %d now probed: (%04X:%04X)\n", 
		iface_desc->desc.bInterfaceNumber,
		id->idVendor, id->idProduct);
	printk(KERN_ALERT "ID->bNumEndpoints: %02X\n",
			 iface_desc->desc.bNumEndpoints);
	printk(KERN_ALERT "ID->bInterfaceClass: %02X\n",
			 iface_desc->desc.bInterfaceClass);

	for(i = 0; i < iface_desc->desc.bNumEndpoints; i++){
		endpoint = &iface_desc->endpoint[i].desc;

		printk(KERN_INFO "ED[%d] -> bEndpointAddress: 0x%02X\n",
			i, endpoint->bEndpointAddress);
		printk(KERN_INFO "ED[%d] -> bmAttributes: 0x%02X\n",
			i, endpoint->bmAttributes);
		printk(KERN_INFO "ED[%d] -> wMaxPacketSize: 0x%04X (%d)\n",
			i, endpoint->wMaxPacketSize,
			endpoint->wMaxPacketSize);
	}

	device = interface_to_usbdev(interface);
	return 0;
}

static void pen_disconnect(struct usb_interface *interface){
	printk(KERN_ALERT "Pen i/f %d now removed...\n", 
		interface->cur_altsetting->desc.bInterfaceNumber);
}

static struct usb_device_id pen_table[]={
	//058f:6387
	{ USB_DEVICE(0x058f, 0x6387)},
	{} /*Terminating entry*/
};
MODULE_DEVICE_TABLE(usb, pen_table);

static struct usb_driver pen_driver = {
	.name = "david's pen driver",
	.id_table = pen_table,
	.probe = pen_probe,
	.disconnect = pen_disconnect,
};

static int __init pen_init(void){
	return usb_register(&pen_driver);
}

static void __exit pen_exit(void){
	usb_deregister(&pen_driver);
}

module_init(pen_init);
module_exit(pen_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("David Bors <daviddvd267@gmail.com>");
MODULE_DESCRIPTION("Usb driver!!");
