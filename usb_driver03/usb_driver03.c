#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>

/* probe function */
static int davids_probe(struct usb_interface *intf, const struct usb_device_id *id){
	printk(KERN_INFO "Usb plugged in... David's driver is taking care of this\n");
	return 0;
}

/* disc function */
static void davids_disconnect(struct usb_interface *intf){
	printk(KERN_INFO "Usb plugged out... Goodbye!\n");
}

/* creating the device id table */
/* vendorId, productId */
static struct usb_device_id davids_table [] = {
	{ USB_DEVICE(0x058f, 0x6387) },
	/* if you want your driver to be called for any usb dev */
	/* { .driver_info = 42 }, */
	{ } /*TERMINATING THE ENTRY*/
};
MODULE_DEVICE_TABLE(usb, davids_table);

/*create the usb driver struct*/
static struct usb_driver davids_driver = {
	//.owner = THIS_MODULE,
	.name = "davids usb driver",
	.id_table = davids_table,
	.probe = davids_probe,
	.disconnect = davids_disconnect,
};

static int __init usb_driver03_init(void){

	//register the driver with the usb subsystem
	int result;
	result = usb_register(&davids_driver);
	if(result)
		printk(KERN_ALERT "usb register failed, error %d", result);

	return result;
}

static void __exit usb_driver03_exit(void){
	//deregister the driver from the usb subsystem
	usb_deregister(&davids_driver);
}

module_init(usb_driver03_init);
module_exit(usb_driver03_exit);
MODULE_LICENSE("GPL");
