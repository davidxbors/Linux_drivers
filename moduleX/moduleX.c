#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h> /*for opening character device*/

MODULE_LICENSE("GPL2");

ssize_t x_read (struct file *pfile, char __user *buffer, size_t length, loff_t *offset){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}
ssize_t x_write (struct file *pfile, const char __user *buffer, size_t length, loff_t *offset){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return length;
}
int x_open (struct inode *pinode, struct file *pfile){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}
int x_close (struct inode *pinode, struct file *pfile){
  printk(KERN_ALERT "Inside %s function!", __FUNCTION__);
  return 0;
}

struct file_operations file_ops_X = {
  .owner   = THIS_MODULE,
  .open    = x_open,
  .read    = x_read,
  .write   = x_write,
  .release = x_close,
};

__init static int moduleX_init(void){
  printk(KERN_ALERT "ModuleX init!\n");
  // register with the kernel and indicate that we are registering a char device drivers
  // register_chrdev(major number, name of the driver, &(ops to be done))
  register_chrdev(240, "Example Char Driver", &file_ops_X);
  return 0;
}

__exit static void moduleX_exit(void){
  printk(KERN_ALERT "ModuleX exit!\n");
  // unregister the char device driver
  // unregister_chrdev(major number, name of the driver)
  unregister_chrdev(240, "Example Char Driver");
}

module_init(moduleX_init);
module_exit(moduleX_exit);
